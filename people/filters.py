class NLNFilter:
    """
        фильтрует по заданному имени и/или фаимлии
    """

    def get_filterexpressions(self, nln):
        result = {}
        nln = nln.split()
        len_nln = len(nln)
        if len_nln == 1:  ###получили только фамилию
            result['last_name__iexact'] = nln[0].strip()
        elif len_nln > 0:  ###получили имя и фамилию
            result['name__iexact'] = nln[0].strip()
            result['last_name__iexact'] = nln[1].strip()
        return result

    def filter_queryset(self, request, queryset, view):
        myfilter = self.get_filterexpressions(view.kwargs['nln'])
        return queryset.filter(**myfilter)
