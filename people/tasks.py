from django.db import IntegrityError
from redis import Redis

from people.models import WorkerModel, PositionModel
from people.serialaizers import WorkerSerialazier
from trood_big_csv.celery import app


@app.task
def only_save(chunk_str=''):
    """
        Функция получает чанк, и записывет его без проверки в БД, в случае ошибки записи, записывает его по частям с валидацией
    """

    r = Redis(host='redis',db=1)

    data_arr = []

    for raw_string in chunk_str.split():
        data_arr.append(get_sub_arr(raw_string, r))

    try:
        WorkerModel.objects.bulk_create(
            WorkerModel(name=name, last_name=last_name, birthday=birthday, position_id=position) for
            name, last_name, birthday, position in data_arr)
        return [len(data_arr), []]
    except IntegrityError:
        for i in range(len(data_arr)):
            data_arr[i] = {'name': data_arr[i][0], 'last_name': data_arr[i][1], 'birthday': data_arr[i][2],
                           'position': data_arr[i][3]}
        return validate_and_save(data_arr, 0, [])


def get_position(pos, r):
    pos_id = r.get(pos)
    if pos_id == None:
        (obj, crated) = PositionModel.objects.get_or_create(name=pos)
        pos_id = obj.id
        r.set(pos, pos_id)
    return int(pos_id)


def get_sub_arr(raw_string, r):
    sub_arr = raw_string.strip('\r').split(';')
    sub_arr.append(get_position(sub_arr.pop(), r))
    return sub_arr


def validate_and_save(arr, ins, not_ins):
    """
        Функция получает массив, сериализует его элементы в модель WorkerModel
        Если успешно - записывает в БД
        иначе разбивает массив пополам и рекурсивно записывает половинки масива
    """
    len_arr = len(arr)
    serializer = WorkerSerialazier(data=arr, many=True)
    if serializer.is_valid():
        serializer.save()
        ins += len_arr
        return [ins, not_ins]
    else:
        if len_arr > 1:
            mid = len_arr // 2
            left_arr = arr[:mid]
            right_arr = arr[mid:]
            (ins, not_ins) = validate_and_save(left_arr, ins, not_ins)
            (ins, not_ins) = validate_and_save(right_arr, ins, not_ins)
        else:
            not_ins.append(arr[0])
            return [ins, not_ins]
    return [ins, not_ins]
