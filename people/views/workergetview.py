from rest_framework import viewsets
from rest_framework.mixins import ListModelMixin

from people.filters import NLNFilter
from people.models import WorkerModel
from people.serialaizers import WorkerSerialazierGet


class WorkerGetView(viewsets.GenericViewSet, ListModelMixin):
    http_method_names = ['get']
    queryset = WorkerModel.objects.all()
    serializer_class = WorkerSerialazierGet
    filter_backends = (NLNFilter,)
    filter_fields = ('name', 'last_name')
