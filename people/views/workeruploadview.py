import time
from copy import copy

import redis
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from drf_chunked_upload.exceptions import ChunkedUploadError
from drf_chunked_upload.views import ChunkedUploadView
from rest_framework import status
from rest_framework.response import Response

from people.models import MyChunkedUpload
from people.serialaizers import MyChunkedUploadSerializer
from people.tasks import only_save


class UploadProcessingView(ChunkedUploadView):
    model = MyChunkedUpload
    serializer_class = MyChunkedUploadSerializer
    r = redis.Redis(host='redis',db=1)

    def check_permissions(self, request):
        ### Без авторризации, для этого выпилим поле user из MyChunkedUpload
        pass

    @classmethod
    def as_view(cls, **initkwargs):
        view = super().as_view(**initkwargs)
        view.csrf_exempt = True
        return view

    @csrf_exempt
    def _post(self, request, pk=None, *args, **kwargs):
        """
            Пришлось полиморфировать этот метод, в описании написано,
            что к ответу будет добавлена data возвращенная из get_response_data,
            по факту это не так
        """
        chunked_upload = None
        if pk:
            upload_id = pk
        else:
            chunked_upload = self._put_chunk(request, *args,
                                             whole=True, **kwargs)
            upload_id = chunked_upload.id

        md5 = request.data.get('md5')

        error_msg = None
        if self.do_md5_check:
            if not upload_id or not md5:
                error_msg = "Both 'id' and 'md5' are required"
        elif not upload_id:
            error_msg = "'id' is required"
        if error_msg:
            raise ChunkedUploadError(status=status.HTTP_400_BAD_REQUEST,
                                     detail=error_msg)

        if not chunked_upload:
            chunked_upload = get_object_or_404(self.get_queryset(),
                                               pk=upload_id)

        self.is_valid_chunked_upload(chunked_upload)

        if self.do_md5_check:
            self.md5_check(chunked_upload, md5)

        chunked_upload.completed()

        self.on_completion(chunked_upload, request)

        data = self.response_serializer_class(chunked_upload, context={'request': request}).data
        data['db_result'] = self.get_response_data(chunked_upload)
        self.r.delete(chunked_upload.id.hex)
        return Response(data, status=status.HTTP_200_OK)

    def _put_chunk(self, request, pk=None, whole=False, *args, **kwargs):
        ###Сделаем глубокую копию объекта, в суперметоде его разберут на чанки при копировании в файл
        ###Правильнее использовать метод chunks(), но у нас же chunk_uploaded, оставим эту задачу клиенту
        chunk_str = copy(request.data[self.field_name]).read().decode()

        chunked_upload = super(UploadProcessingView, self)._put_chunk(request, pk, whole, *args, **kwargs)

        ### остальное сделаем здесь, т.к. нам понабится chunked_upload, нужен его id
        ### также в chunked_upload будем складывать в список результаты тасков
        prev_str = self.r.get(chunked_upload.id.hex)
        if prev_str is None:
            prev_str = ''
        else:
            prev_str = prev_str.decode()

        chunk_str = prev_str + chunk_str
        prev_str = ''
        content_range = request.META.get('HTTP_CONTENT_RANGE', '')
        match = self.content_range_pattern.match(content_range)
        total = int(match.group('total'))
        if chunked_upload.offset != total:
            while len(chunk_str) > 0 and chunk_str[-1] != '\n':
                prev_str = chunk_str[-1] + prev_str
                chunk_str = chunk_str[:-1]
            if len(chunk_str) == 0:
                chunk_str = prev_str
                prev_str = ''
        self.r.set(chunked_upload.id.hex, prev_str)
        chunked_upload.tasks.append(only_save.delay(chunk_str))
        return chunked_upload

    def get_response_data(self, chunked_upload):
        while not chunked_upload.ready():
            time.sleep(1)

        count_insert = 0
        not_ins_arr = []
        for each in chunked_upload.tasks:
            (ins, not_ins) = each.get()
            count_insert += ins
            not_ins_arr += not_ins

        """
            Затраченное время немного меньше настоящего, потому что chunked_upload создается после
            приема первого чанка
        """
        return {'inserted': count_insert, 'time': time.time() - chunked_upload.created_at.timestamp(),
                'not inserted': not_ins_arr}
