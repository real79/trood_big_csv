import hashlib
from os import fstat

import redis
from celery.contrib.testing.worker import start_worker
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, Client, TransactionTestCase, client

from trood_big_csv.celery import app

INPUT_FILE = 'people/test.csv'
UPLOAD_FILENAME = 'test.csv'
CHUNK_FILENAME = 'people/test_csv.temp'
CHUNK_SIZE = 18000


class TestWorkerGetView(TestCase):
    fixtures = ['positions.json', 'workers.json']

    def test_last_name_request(self):
        response = self.client.get('http://testserver/api/Anisimov')
        assert response.status_code == 200
        assert response.rendered_content == b'[{"id":1,"position":"programmer","name":"Aleksey","last_name":"Anisimov","birthday":"1979-10-15"}]'

    def test_name_last_name_request(self):
        response = self.client.get('http://testserver/api/Aleksey   Anisimov')
        assert response.status_code == 200
        assert response.rendered_content == b'[{"id":1,"position":"programmer","name":"Aleksey","last_name":"Anisimov","birthday":"1979-10-15"}]'

    def test_wrong_request(self):
        response = self.client.get('http://testserver/api/fsdgfsd')
        assert response.status_code == 200
        assert response.content == b'[]'


class TestWorkerUploadView(TransactionTestCase):
    fixtures = ['positions.json', 'workers.json']

    @classmethod
    def setUpClass(cls):
        r = redis.Redis(host='redis',db=1)
        r.flushall()
        super().setUpClass()
        cls.celery_worker = start_worker(app)
        cls.celery_worker.__enter__()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls.celery_worker.__exit__(None, None, None)
        # sleep(5)

    def test_chunked_upload(self):
        end = -1
        hash_md5 = hashlib.md5()
        c = Client()
        url = 'http://testserver/api/upload/'
        with open(INPUT_FILE, 'rb') as f:
            file_size = fstat(f.fileno()).st_size
            for chunk in iter(lambda: f.read(CHUNK_SIZE), b''):
                hash_md5.update(chunk)
                start = end + 1
                end = start + len(chunk) - 1
                upload_file = SimpleUploadedFile(CHUNK_FILENAME, chunk, content_type='text/csv')
                data = client.encode_multipart(client.BOUNDARY, {'file': upload_file, 'filename': UPLOAD_FILENAME})
                header = {'HTTP_CONTENT_RANGE': f'bytes {start}-{end}/{file_size}'}
                response = c.put(path=url, data=data, content_type=client.MULTIPART_CONTENT, **header)

                assert response.status_code == 200
                url = response.data['url']
            response = c.post(path=url, data={'md5': hash_md5.hexdigest()})
            assert response.status_code == 200
            assert response.data['db_result']['inserted'] == 945
            assert response.data['db_result']['not inserted'] == [
                {'name': 'Aleksey', 'last_name': 'Anisimov', 'birthday': '1979-10-15', 'position': 1}]
