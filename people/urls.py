from django.conf.urls import url

from people.views.workergetview import WorkerGetView
from people.views.workeruploadview import UploadProcessingView

urlpatterns = [
    url('upload/(?P<pk>\S+)', UploadProcessingView.as_view(), name='chunkedupload-detail'),
    url('upload', UploadProcessingView.as_view(), name='chunkedupload-begin'),
    url(r'^(?P<nln>[A-Za-zА-Яа-я ]+[^\/])$', WorkerGetView.as_view({'get': 'list'}))
]
