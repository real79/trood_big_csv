from rest_framework import serializers
from rest_framework.reverse import reverse

from people.models import WorkerModel, PositionModel, MyChunkedUpload


class PositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = PositionModel
        fields = ('name', 'id')


class WorkerSerialazier(serializers.ModelSerializer):
    position = serializers.PrimaryKeyRelatedField(queryset=PositionModel.objects.all())

    class Meta:
        model = WorkerModel
        fields = '__all__'


class WorkerSerialazierGet(serializers.ModelSerializer):
    position = serializers.PrimaryKeyRelatedField(queryset=PositionModel.objects.all(), source='position.name')

    class Meta:
        model = WorkerModel
        fields = '__all__'


class MyChunkedUploadSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()

    def create(self, validated_data):
        """
        Т.к. у нас возможна загрузка без авторизации, и мы убрали это свойство из модели, уберем User из validated_data
        """
        validated_data.pop('user')
        return super(MyChunkedUploadSerializer, self).create(validated_data=validated_data)

    def get_url(self, obj):
        return reverse('chunkedupload-detail',
                       kwargs={'pk': obj.id},
                       request=self.context['request'])

    class Meta:
        model = MyChunkedUpload
        fields = '__all__'
        read_only_fields = ('status', 'completed_at')
