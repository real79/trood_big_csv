from django.db import models
from drf_chunked_upload.models import ChunkedUpload


class MyChunkedUpload(ChunkedUpload):
    tasks = []
    user = None

    def ready(self):
        return all(task.ready() for task in self.tasks)

    class Meta:
        abstract = False


class PositionModel(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        unique_together = ('name',)

    def __repr__(self):
        return self.name


class WorkerModel(models.Model):
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    birthday = models.DateField()
    position = models.ForeignKey(PositionModel, on_delete=models.CASCADE, default=None)

    class Meta:
        unique_together = (('name', 'last_name', 'birthday'),)

    def __str__(self):
        return self.name + ' ' + self.last_name
