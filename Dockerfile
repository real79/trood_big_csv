FROM python:3.7
ENV PYTHONUNBUFFERED 1
RUN mkdir /troot_big_csv
WORKDIR /trood_big_csv
COPY . /trood_big_csv
RUN pip install -r requirements.txt
EXPOSE 8000