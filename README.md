This example is designed to load a large csv file into the postgresql database.
The project uses the drf-chunked-upload application, the file is received from the client in parts or in whole,
a description of working with drf-chunked-upload is here https://github.com/jkeifer/drf-chunked-upload/blob/master/README.rst
The endpoint '/api/upload' accepts a post or put request (see the drf-chunked-upload documentation) and creates celery tasks,
which upload information to the database, upon completion of the download, information is returned on the number of downloaded records,
time spent processing and records that were not loaded due to IntegrityKeyError
The endpoint '/api/some name with lastname or lastname only' is case insensitive (for example /api/anisimov or /api/aleksey anisimov) 
displays information about the employee found or empty if the employee not found in database 

1. Install docker, docker-compose:
https://docs.docker.com/install/
https://docs.docker.com/compose/install/
 
2. Execute:
cd ~ && mkdir some_dir && cd some_dir && git clone https://gitlab.com/real79/trood_big_csv && cd trood_big_csv && mkdir volumes ./volumes/db
 
3. Run container with tests:
docker-compose -f docker-compose.test.yml up
 
4. if tests OK then stop container ctrl+C
 
5. Run container with app:
docker-compose up